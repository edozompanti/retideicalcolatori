#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "database.h"

void insert(Database * database, Persona * persona) {
    IndexNodeString *nameNode = (IndexNodeString *)malloc(sizeof(IndexNodeString));
    nameNode->value = (char*) malloc(sizeof(char)*strlen(persona->name));
    strcpy(nameNode->value,persona->name);
    nameNode->left = NULL;
    nameNode->right = NULL;
    nameNode->persona=persona;

    IndexNodeString *surnameNode = (IndexNodeString *)malloc(sizeof(IndexNodeString));
    surnameNode->value = (char*) malloc(sizeof(char)*strlen(persona->surname));
    strcpy(nameNode->value,persona->surname);
    surnameNode->left = NULL;
    surnameNode->right = NULL;
    surnameNode->persona = persona;

    IndexNodeString *addressNode = (IndexNodeString *)malloc(sizeof(IndexNodeString));
    surnameNode->value = (char*) malloc(sizeof(char)*strlen(persona->address));
    strcpy(nameNode->value,persona->address);
    addressNode->left = NULL;
    addressNode->right = NULL;
    addressNode->persona = persona;

    IndexNodeInt *ageNode = (IndexNodeInt *)malloc(sizeof(IndexNodeInt));
    ageNode->value = persona->age;
    ageNode->left = NULL;
    ageNode->right = NULL;
    ageNode->persona = persona;

    if (database->name == NULL) {
        database->name = nameNode;
    } else {
        IndexNodeString *current = database->name;
        while (1) {
            int cmp = strcmp(persona->name, current->value);
            if (cmp < 0) {
                if (current->left == NULL) {
                    current->left = nameNode;
                    break;
                }
                current = current->left;
            } else if (cmp > 0) {
                if (current->right == NULL) {
                    current->right = nameNode;
                    break;
                }
                current = current->right;
            } else {
                break;
            }
        }
    }

    if (database->surname == NULL) {
        database->surname = surnameNode;
    } else {
        IndexNodeString *current = database->surname;
        while (1) {
            int cmp = strcmp(persona->surname, current->value);
            if (cmp < 0) {
                if (current->left == NULL) {
                    current->left = surnameNode;
                    break;
                }
                current = current->left;
            } else if (cmp > 0) {
                if (current->right == NULL) {
                    current->right = surnameNode;
                    break;
                }
                current = current->right;
            } else {
                break;
            }
        }
    }

    if (database->address == NULL) {
        database->address = addressNode;
    } else {
        IndexNodeString *current = database->address;
        while (1) {
            int cmp = strcmp(persona->address, current->value);
            if (cmp < 0) {
                if (current->left == NULL) {
                    current->left = addressNode;
                    break;
                }
                current = current->left;
            } else if (cmp > 0) {
                if (current->right == NULL) {
                    current->right = addressNode;
                    break;
                }
                current = current->right;
            } else {
                break;
            }
        }
    }

    if (database->age == NULL) {
        database->age = ageNode;
    } else {
        IndexNodeInt *current = database->age;
        while (1) {
            if (persona->age < current->value) {
                if (current->left == NULL) {
                    current->left = ageNode;
                    break;
                }
                current = current->left;
            } else if (persona->age > current->value) {
                if (current->right == NULL) {
                    current->right = ageNode;
                    break;
                }
                current = current->right;
            } else {
                break;
            }
        }
    }
}

Persona* findByName(Database * database, char * name) {
    IndexNodeString *current = database->name;
    while (current != NULL) {
        int cmp = strcmp(name, current->value);
        if (cmp == 0) {
            return current->persona; 
        } else if (cmp < 0) {
            current = current->left; 
        } else {
            current = current->right; 
        }
    }
    return NULL; 
}

Persona* findBySurname(Database * database, char * surname) {
    IndexNodeString *current = database->surname;
    while (current != NULL) {
        int cmp = strcmp(surname, current->value);
        if (cmp == 0) {
            return current->persona;
        } else if (cmp < 0) {
            current = current->left; 
        } else {
            current = current->right; 
        }
    }
    return NULL; 
}

Persona* findByAddress(Database * database, char * address) {
    IndexNodeString *current = database->address;
    while (current != NULL) {
        int cmp = strcmp(address, current->value);
        if (cmp == 0) {
            return current->persona;
        } else if (cmp < 0) {
            current = current->left; 
        } else {
            current = current->right; 
        }
    }
    return NULL; 
}

Persona* findByAge(Database * database, int age) {
    IndexNodeInt *current = database->age;
    while (current != NULL) {
        if (age == current->value) {
            return current->persona; 
        } else if (age < current->value) {
            current = current->left; 
        } else {
            current = current->right; 
        }
    }
    return NULL; 
}

//optional

void freeMemory_NodeString(IndexNodeString* node){
    if(node->left!=NULL) freeMemory_NodeString(node->left);
    if(node->right!=NULL) freeMemory_NodeString(node->right);
    free(node->value);
    free(node);
}

void freeMemory_NodeInt(IndexNodeInt* node){
    if(node->left!=NULL) freeMemory_NodeInt(node->left);
    if(node->right!=NULL)freeMemory_NodeInt(node->right);
    free(node);
}

void free_Tree(Database* database){
    freeMemory_NodeString(database->name);
    freeMemory_NodeString(database->surname);
    freeMemory_NodeString(database->address);
    freeMemory_NodeInt(database->age);
    free(database);
}